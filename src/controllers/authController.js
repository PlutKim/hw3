const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const { userJoiSchema } = require('../models/User')

const {
  saveUser,
  getUserByEmail,
} = require('../services/usersService')

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body
  await userJoiSchema.extract(['email']).validateAsync(email)
  await userJoiSchema.extract(['password']).validateAsync(password)
  await userJoiSchema.extract(['role']).validateAsync(role)
  await saveUser({ email, password, role })
  return res
    .status(200)
    .json({ message: 'Profile created successfully' })
}

const loginUser = async (req, res, next) => {
  const { email, password } = req.body
  await userJoiSchema.extract(['email']).validateAsync(email)
  const user = await getUserByEmail(email)
  if (!user) {
    throw Error(`User is undefinded`)
  }
  await userJoiSchema.extract(['password']).validateAsync(password)
  const isPasswordCorrect = await bcrypt.compare(
    String(password),
    String(user.password)
  )
  if (!isPasswordCorrect) {
    throw Error('Wrong password, try again')
  }
  const payload = {
    email: user.email,
    userID: user._id,
    role: user.role,
    created_date: user.created_date
  }
  const jwt_token = jwt.sign(payload, 'secret-jwt-key')
  return res.status(200).json({
    'jwt_token': jwt_token
  })
}

module.exports = {
  registerUser,
  loginUser
}
